#include <SoftwareSerial.h>
#include <Wire.h>
#include <LiquidCrystal_PCF8574.h>
#include <Keypad.h>
#include <EEPROM.h>

//EEPROM definitions
const int ARM_STATE_ADDR=0;
const int PASSWORD_ADDR=1;//save password from address 1 to 6
const int PHONE_NUM_ADDR=7;
const byte ARMED=1;
const byte DISARMED=2;

//All pin numbers are mapped to arduino uno board
int motionSensor = 2;//PD2
int outputLed = 9;
int softwareSerialRX=3;
int softwareSerialTX=4;

//Necessary flags
boolean motionDetected = false;
boolean loginState= false;
boolean armState=false;
boolean clearLcd=true;//used in functions to lcd clear only once
boolean inputTaken=false;

//LCD page definitions
byte const MENU=0;
byte const CPASS=1;
byte const CNUM=2;
byte const AD=3;
byte const LOGOUT=4;
byte pageNo=0;

//Library objects
SoftwareSerial mySerial(softwareSerialRX, softwareSerialTX);
LiquidCrystal_PCF8574 lcd(0x3F);
 
//arrays
//char gsmSerial[20];
char inputPassword[6]={'1','2','3','4','5','6'};
int passwordIndex=0;
char password[6]={'1','2','3','4','5','6'};

char phoneNumber[12]={'0','1','5','2','1','4','8','6','1','1','5','\0'};
char inputPhoneNumber[11]={'0','1','5','2','1','4','8','6','1','1','5'};
int phoneNumberIndex=0;

//keypad definitions
const byte ROWS = 4; //four rows
const byte COLS = 4; //four columns
//define the cymbols on the buttons of the keypads
char hexaKeys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','B'},
  {'*','0','#','D'}
};
byte rowPins[ROWS] = {5, 6, 7, 8}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {10, 11, 12, 13}; //connect to the column pinouts of the keypad

//initialize an instance of class NewKeypad
Keypad myKeypad = Keypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS); 

void setup() {
 
pinMode(motionSensor, INPUT);
pinMode(outputLed, OUTPUT);
attachInterrupt(digitalPinToInterrupt(motionSensor), handleMotion , RISING);
Serial.begin(9600);
mySerial.begin(9600);
lcd.begin(16, 2); // initialize the lcd
lcd.setBacklight(255);

//uncomment the below three lines to reset default credentials 
//writeDeviceStateEEPROM(true);
//writePasswordEEPROM();  
//writePhoneNumEEPROM();


delay(1000);
loadDataFromEEPROM();
if(armState)
stateIndication(true);
else
stateIndication(false);
}

void loop() {
  if(!loginState){
    lcdGivePassword();
    readPassword();
    }else{
      switch(pageNo){
        case MENU:
        
        lcdMenu();//shows the different options and takes one char input to go there
        break;

        case CPASS:
        lcdUpdatePassword();//Changes the password char array and returns to menu page upon a valid password update and # button
        break;

        case CNUM:
        lcdUpdateNumber();//changes the phoneNumber char array and returns to menu page upon a valid phone number and # button
        break;

        case AD:
        lcdArm();//changes device state and returns to menu page upon a valid state and # button
        break;

        case LOGOUT:
       
        lcdLogout();//confirms logout upon pressing # by setting loginState false  or returns to menu page
        break;

        default:
        break;
        }
      }
  
  if (motionDetected & armState)
  {
    motionDetected=false;
    blinky();
    Serial.println("sending sms");
    SendMessage();
    
    }

}

void stateIndication(boolean state){
  if (state)
  digitalWrite(outputLed , HIGH);
  else
  digitalWrite(outputLed , LOW);
  }

void blinky(){
  digitalWrite(outputLed , LOW);
    delay(500);
    digitalWrite(outputLed , HIGH);
    delay(500);
    digitalWrite(outputLed , LOW);
    delay(500);
    digitalWrite(outputLed , HIGH);
    delay(500);
  }
void handleMotion(){
  if(armState)
  motionDetected = true;
  }
//---------------------------GSM FUNCTIONS------------------------------------------------
void SendMessage()
 {
  mySerial.println("AT+CMGF=1");    //Sets the GSM Module in Text Mode
  delay(1000);  // Delay of 1000 milli seconds or 1 second
  //String pN((char*)phoneNumber);
  String pN=phoneNumber;
  mySerial.println("AT+CMGS=\"+88"+pN+"\"\r"); // Replace x with mobile number
  delay(1000);
  mySerial.println("Burgler Attack");// The SMS text you want to send
  delay(100);
   mySerial.println((char)26);// ASCII code of CTRL+Z
  delay(1000);
}


//****************************LCD Section Start***************************************//

void lcdGivePassword(){
     lcd.home(); 
    lcd.print("Give Password");
  }
void lcdPassMatched(){
  lcd.setCursor(0,1);
  lcd.print(F("Logged In!"));
  delay(1500);
  lcd.clear();
  }
  void lcdPassDidntMatch(){
  lcd.setCursor(0,1);
  lcd.print(F("Wrong Password!!"));
  delay(1500);
  lcd.clear();
  }
  //shows the different options and takes one char input to go there
void lcdMenu(){
  if(clearLcd)
  {lcd.clear();
  clearLcd=false;
  }
  
  lcd.setCursor(0,0);
  lcd.print(F("1->U PW"));
  lcd.setCursor(8,0);
  lcd.print(F("2->U MN"));
  lcd.setCursor(0,1);
  lcd.print(F("3->A DA"));
  lcd.setCursor(8,1);
  lcd.print(F("4->LOGOUT"));
  char pKey =myKeypad.getKey();
 if(pKey!= NO_KEY){ 
  if (pKey=='1')
  {
    pageNo=CPASS;
    clearLcd=true;
  }
  else if(pKey=='2')
  {
    pageNo=CNUM;
    clearLcd=true;
  }
  
   else if(pKey=='3')
  {
    pageNo=AD;
    clearLcd=true;
  }
   else if(pKey=='4')
  {
    pageNo=LOGOUT;
    clearLcd=true;
  }
  else
  {
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print(F("INVALID KEY!"));
    }
   }
 }

 void lcdUpdatePassword(){
  if(clearLcd)
  {lcd.clear();
  clearLcd=false;
  }
  lcd.setCursor(0,0);
  lcd.print(F("Size 6Ch"));
  char pKey =myKeypad.getKey();
    if(pKey=='#' && inputTaken==true){
     inputTaken=false;
     writePasswordEEPROM();
     loadDataFromEEPROM();
     returnToMenu();
     }
    else if(pKey!=NO_KEY){
      inputPassword[passwordIndex]=pKey;
      lcd.setCursor(passwordIndex,1);
      lcd.print(String(inputPassword[passwordIndex]));
      passwordIndex++;
      if(passwordIndex>5)
      {passwordIndex=6;
      inputTaken=true;
      }
      }
  }

void lcdUpdateNumber(){
  if(clearLcd)
  {lcd.clear();
  clearLcd=false;
  }
  
  lcd.setCursor(0,0);
  lcd.print(F("Size11Ch"));
  char pKey =myKeypad.getKey();
    if(pKey=='#' && inputTaken==true){
     inputTaken=false;
     writePhoneNumEEPROM();
     loadDataFromEEPROM();
     returnToMenu();
     }
    else if(pKey!=NO_KEY){
      inputPhoneNumber[phoneNumberIndex]=pKey;
      lcd.setCursor(phoneNumberIndex,1);
      lcd.print(String(inputPhoneNumber[phoneNumberIndex]));
      phoneNumberIndex++;
      if(phoneNumberIndex>10)
      {phoneNumberIndex=0;
      inputTaken=true;
      }
      }
  }

void lcdArm(){
  if(clearLcd)
  {lcd.clear();
  clearLcd=false;
  }
  lcd.setCursor(0,0);
  lcd.print(F("A-> ARM"));
  lcd.setCursor(0,1);
  lcd.print(F("D-> DISARM"));
  char pKey =myKeypad.getKey();
  if(pKey=='A')
  {
    armState=true;
    writeDeviceStateEEPROM(true);
    stateIndication(true);
    returnToMenu();
    }
    else if(pKey=='D'){
      armState=false;
      writeDeviceStateEEPROM(false);
      stateIndication(false);
      returnToMenu();
      }
      
  }

 void lcdLogout(){
 if(clearLcd)
  {lcd.clear();
  clearLcd=false;
  }
  
  lcd.setCursor(0,0);
  lcd.print(F("PRESS #"));
   char pKey =myKeypad.getKey();
    if(pKey=='#'){
      loginState=false;
      }
      else if(pKey!=NO_KEY){
        returnToMenu();
        }
      
  }

  void returnToMenu(){
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print(F("RETURNING TO MENU"));
        delay(1000);
        pageNo=MENU;
        clearLcd=true;
    }
//****************************LCD Section End*****************************************//

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^password Section start^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^//
  void readPassword(){
    char pKey =myKeypad.getKey();
    if(pKey=='#'){
     if(matchPassword())
     {
      lcdPassMatched();
      //change login state here
      loginState=true;
      pageNo=MENU;
      clearLcd=true;
      }else{
        passwordIndex=0;
        lcdPassDidntMatch();
          }
     }
    else if(pKey!=NO_KEY){
      inputPassword[passwordIndex]=pKey;
      lcd.setCursor(passwordIndex,1);
      lcd.print(String(inputPassword[passwordIndex]));
      passwordIndex++;
      if(passwordIndex>5)
      {passwordIndex=0;
      }
      }
   
    }
    boolean matchPassword(){
      int i=0;
      for (i=0; i<6;i++){
        if(inputPassword[i]==password[i])
        {}
        else{
          return false;}
        }
        return true;
      }
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^password Section end^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^//


//^^^^^^^^^^^^^^^^^^^^^^^^^^^^EEPROM Section start^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^//
void loadDataFromEEPROM(){
  int i=0;
  byte state=255;
  //Read arm state
state=EEPROM.read(ARM_STATE_ADDR);

  
if(state == 1){
  armState=true;
  }else{
    armState=false;
    }
  //Read password
  int pass=PASSWORD_ADDR;
for(i=0;i<6;i++){
  password[i]=char(EEPROM.read(pass));
  pass++;
  }
  //Read phone number
  int phn=PHONE_NUM_ADDR;
for(i=0;i<11;i++){
  phoneNumber[i]=char(EEPROM.read(phn));
  phn++;
  }

  //uncomment the following section to see data saved in EEPROM
  /*lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(F("state"));
  lcd.setCursor(0,1);
  lcd.print(state);
  delay(1000);

  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(F("Password"));
  for(i=0;i<6;i++){
    lcd.setCursor(i,1);
    lcd.print(password[i]);
    }
  delay(1000);


  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(F("Phone no"));
  for(i=0;i<11;i++){
    lcd.setCursor(i,1);
    lcd.print(phoneNumber[i]);
    }
  delay(1000);
  lcd.clear();*/
  }

void writeDeviceStateEEPROM(boolean state){
  if(state){
    EEPROM.write(ARM_STATE_ADDR,ARMED);
    }else{
      EEPROM.write(ARM_STATE_ADDR,DISARMED);
      }
  }

void writePasswordEEPROM(){
    int i=0;
    int pass=PASSWORD_ADDR;
    for(i=0;i<6;i++){
      EEPROM.write(pass,byte(inputPassword[i]));
      pass++;
      //delay(2);
      }
    }

 void writePhoneNumEEPROM(){
  int i=0;
    int phn=PHONE_NUM_ADDR;
    for(i=0;i<11;i++){
      EEPROM.write(phn,byte(inputPhoneNumber[i]));
      phn++;
      //delay(2);
      }
  }


      
